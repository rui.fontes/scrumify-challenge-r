package pt.com.scrumify.controllers;

import java.util.List;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.services.OccupationService;
import pt.com.scrumify.database.entities.TeamContract;
import pt.com.scrumify.database.entities.TeamMember;
import pt.com.scrumify.database.entities.TeamMemberPK;
import pt.com.scrumify.database.services.ResourceService;
import pt.com.scrumify.database.services.SubAreaService;
import pt.com.scrumify.database.services.TeamContractService;
import pt.com.scrumify.database.services.TeamMemberService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.entities.TeamMemberView;
import pt.com.scrumify.entities.TeamView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.TeamsHelper;

@Controller
public class TeamMembersController {
   
   @Autowired
   private OccupationService occupationsService;
   @Autowired
   private ResourceService resourcesService;
   @Autowired
   private SubAreaService subAreaService;
   @Autowired
   private TeamContractService teamContractsService; 
   @Autowired
   private TeamMemberService teamMembersService;
   @Autowired
   private TeamsHelper teamsHelper; 
   @Autowired
   private TeamService teamsService;

   
   /*
    * TAB TEAM INFO
    */
   @ApiOperation("Mapping to respond ajax requests for team info.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_READ + "') and @SecurityService.isMember('Team', #team)")
   @GetMapping( value = ConstantsHelper.MAPPING_TEAMS_AJAX + ConstantsHelper.MAPPING_PARAMETER_TEAM )
   public String teaminfoTab(Model model, @PathVariable int team ) {
      TeamView teamView = new TeamView(teamsService.getOne(team));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, teamView);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, subAreaService.listSubarea(teamView.getSubArea()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, teamsHelper.listOfResources(team));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MEMBERS, teamMembersService.getByTeam(this.teamsService.getOne(team)));
      
      return ConstantsHelper.VIEW_TEAMS_INFO;
   }
   
   /*
    * UPDATE SUBAREA IN TEAM
    */
   @ApiOperation("Mapping to update Team Subarea.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_UPDATE + "') and @SecurityService.isMember('Team', #team)")
   @GetMapping(value = ConstantsHelper.TEAM_API_MAPPING_UPDATE_SUBAREA + ConstantsHelper.MAPPING_PARAMETER_TEAM + ConstantsHelper.MAPPING_PARAMETER_SUBAREA)
   public String updateSubarea(Model model, @PathVariable int team, @PathVariable int subarea) {
      
      TeamView teamView = new TeamView(this.teamsService.getOne(team));
      List<TeamContract> teamcontracts  = this.teamContractsService.getByTeam(this.teamsService.getOne(team));
         for (int i = 0; i < teamcontracts.size(); i++){
            teamContractsService.delete(teamcontracts.get(i));
         }
      
      teamView.setSubArea(this.subAreaService.getOne(subarea));
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, teamsService.save(teamView.translate()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, subAreaService.listSubarea(teamView.getSubArea()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, teamsHelper.listOfResources(team));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MEMBERS, teamMembersService.getByTeam(teamsService.getOne(team)));
      
      return ConstantsHelper.VIEW_TEAMS_INFO;
   }
   
  /*
   * ADD MEMBER TO TEAM 
   */
  @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_UPDATE + "') and @SecurityService.isMember('Team', #team)")
  @GetMapping(value = ConstantsHelper.MAPPING_TEAMMEMBERS + ConstantsHelper.MAPPING_PARAMETER_TEAM + ConstantsHelper.MAPPING_PARAMETER_RESOURCE)
  public String add(Model model, @PathVariable int team, @PathVariable int resource) {
     TeamMemberPK teamMemberPK = new TeamMemberPK();
     teamMemberPK.setResource(this.resourcesService.getOne(resource));
     teamMemberPK.setTeam(this.teamsService.getOne(team));
     
     TeamMemberView teamMember = new TeamMemberView();
     teamMember.setPk(teamMemberPK);
     
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMMEMBER, teamMember);
     model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_OCCUPATIONS, this.occupationsService.getAll());
                 
     return ConstantsHelper.VIEW_TEAMMEMBERS_SAVE;
  }
   
  /*
   * EDIT TEAM MEMBER 
   */
  @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_UPDATE + "') and @SecurityService.isMember('Team', #team)")
  @GetMapping(value = ConstantsHelper.MAPPING_TEAMMEMBERS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_TEAM + ConstantsHelper.MAPPING_PARAMETER_RESOURCE)
  public String update(Model model, @PathVariable int team, @PathVariable int resource) {
    TeamMemberPK teamMemberPK = new TeamMemberPK();
    teamMemberPK.setResource(this.resourcesService.getOne(resource));
    teamMemberPK.setTeam(this.teamsService.getOne(team));
    
    TeamMemberView teamMember = new TeamMemberView(this.teamMembersService.getOne(teamMemberPK));
    
    model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMMEMBER, teamMember);
    model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_OCCUPATIONS, this.occupationsService.getAll());
                
    return ConstantsHelper.VIEW_TEAMMEMBERS_SAVE;
 }
  
   /*
    * SAVE TEAM MEMBER 
    */
   @PostMapping(value = ConstantsHelper.MAPPING_TEAMMEMBERS_SAVE)
   public String save(Model model, @Valid TeamMemberView teamMember) {
      this.teamMembersService.save(teamMember.translate());

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MEMBERS, this.teamMembersService.getByTeam(teamMember.getPk().getTeam()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, this.teamsHelper.listOfResources(teamMember.getPk().getTeam().getId()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, this.teamsService.getOne(teamMember.getPk().getTeam().getId()));
                  
      return ConstantsHelper.TEAMMEMBERS_VIEW_INDEX;
   }

   /*
    * REMOVE TEAM MEMBER 
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_UPDATE + "') and @SecurityService.isMember('Team', #team)")
   @GetMapping(value = ConstantsHelper.MAPPING_TEAMMEMBERS_DELETE + ConstantsHelper.MAPPING_PARAMETER_TEAM + ConstantsHelper.MAPPING_PARAMETER_RESOURCE)
   public String delete(Model model, @PathVariable int team, @PathVariable int resource) {
      TeamMemberPK teamMemberPK = new TeamMemberPK();
      teamMemberPK.setResource(this.resourcesService.getOne(resource));
      teamMemberPK.setTeam(this.teamsService.getOne(team));
      
      TeamMember teamMember = new TeamMember();
      teamMember.setPk(teamMemberPK);
      teamMembersService.delete(teamMember);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MEMBERS, this.teamMembersService.getByTeam(this.teamsService.getOne(team)));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_RESOURCES, this.teamsHelper.listOfResources(team));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, this.teamsService.getOne(team));

      return ConstantsHelper.TEAMMEMBERS_VIEW_INDEX;
   }      

}
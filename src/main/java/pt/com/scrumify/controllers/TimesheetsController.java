package pt.com.scrumify.controllers;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.Absence;
import pt.com.scrumify.database.entities.CompensationDay;
import pt.com.scrumify.database.entities.Holiday;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.TimesheetDay;
import pt.com.scrumify.database.entities.TimesheetStatus;
import pt.com.scrumify.database.entities.Vacation;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.services.AbsenceService;
import pt.com.scrumify.database.services.CompensationDayService;
import pt.com.scrumify.database.services.HolidayService;
import pt.com.scrumify.database.services.TimesheetDayService;
import pt.com.scrumify.database.services.TimesheetService;
import pt.com.scrumify.database.services.VacationService;
import pt.com.scrumify.database.services.WorkItemWorkLogService;
import pt.com.scrumify.database.services.YearService;
import pt.com.scrumify.entities.AbsenceView;
import pt.com.scrumify.entities.TimesheetView;
import pt.com.scrumify.entities.VacationView;
import pt.com.scrumify.entities.WorkItemWorkLogView;
import pt.com.scrumify.helpers.AbsencesHelper;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.DatesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.TimesheetsHelper;
import pt.com.scrumify.helpers.VacationsHelper;
import pt.com.scrumify.helpers.WorkItemWorkLogsHelper;
import pt.com.scrumify.helpers.YearsHelper;

@Controller
public class TimesheetsController {   
   @Autowired
   private CompensationDayService  compensationDaysService;
   @Autowired
   private HolidayService holidaysService;
   @Autowired
   private TimesheetDayService timesheetDayService;  
   @Autowired
   private TimesheetService timesheetService;   
   @Autowired
   private VacationService vacationsService;   
   @Autowired
   private AbsenceService absencesService;
   @Autowired
   private WorkItemWorkLogService workItemWorkLogService;
   @Autowired
   private YearService yearsService;
   @Autowired
   private AbsencesHelper absencesHelper;
   @Autowired
   private TimesheetsHelper timesheetHelper;
   @Autowired
   private VacationsHelper vacationsHelper;
   @Autowired
   private WorkItemWorkLogsHelper workLogsHelper;
   @Autowired
   private YearsHelper yearsHelper;  

   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }

   /*
    * Index
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_TIMESHEET)
   public String index(Model model) {
      LocalDate today = LocalDate.now();
      
      return  ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_SLASH + today.getYear() + ConstantsHelper.MAPPING_SLASH + String.format("%2s", today.getMonthValue()).replace(' ', '0');
   }
   
   /*
    * TODO: code review
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_PARAMETER_YEAR_MONTH)
   public String list(Model model, @PathVariable int year, @PathVariable Integer month) {      
      Resource resource = ResourcesHelper.getResource();
      Year yearEntity = yearsService.getOne(year);
      List<TimesheetDay> days = timesheetDayService.findByYearAndMonthOrderByDate(yearEntity, month,resource);

      if (days.isEmpty()) {
         days = timesheetHelper.createNewTimesheets1(resource, yearEntity, month);
      }    
      List<Timesheet> sheets = timesheetService.getSheets(resource, yearEntity, month);
      Timesheet timesheet = timesheetHelper.findTimeSheetOfToday(sheets);
      if(timesheet == null){
         timesheet = new Timesheet();
      }
      timesheet.setMonth(month);
      timesheet.setYear(yearEntity.getId());
      
      List<Year> years = yearsService.getAllActive();
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CURRENT_YEAR, yearEntity.getId());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CURRENT_MONTH, month);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, years);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETDAYS, days);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, sheets);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET, timesheet);
      Date dateApprovedorReviewed = timesheetService.dateLastTimeSheetApprovedorReviewed(ResourcesHelper.getResource());
      String formattedDate = null;
      if(dateApprovedorReviewed != null){
          formattedDate = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME).format(dateApprovedorReviewed);
      }
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_DATE_LASTDAYAPPROVED, formattedDate);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_LIMIT_DATE, yearsHelper.getMaxActiveYear());
      
//      if(timesheet.getId() != null){
//         timesheetHelper.messageInfo(model, timesheet);
//      }
   
      return ConstantsHelper.VIEW_TIMESHEET_READ;
   }
   
   /*
    * TODO: code review
    */
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_READ + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_TIMESHEET)
   public String post(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET) TimesheetView timesheetView) {      
      Resource resource = ResourcesHelper.getResource();
      Year yearEntity = yearsService.getOne(timesheetView.getYear());
      List<TimesheetDay> days = timesheetDayService.findByYearAndMonthOrderByDate(yearEntity, timesheetView.getMonth(),resource);

      if (days.isEmpty()) {
         days = timesheetHelper.createNewTimesheets1(resource, yearEntity, timesheetView.getMonth());
      }    
      List<Timesheet> sheets = timesheetService.getSheets(resource, yearEntity, timesheetView.getMonth());
      Timesheet timesheet = timesheetHelper.findTimeSheetOfToday(sheets);
      if(timesheet == null){
         timesheet = new Timesheet();
      }
      
      timesheet.setMonth(timesheetView.getMonth());
      timesheet.setYear(yearEntity.getId());
      
      List<Year> years = yearsService.getAllActive();
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CURRENT_YEAR, yearEntity.getId());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_CURRENT_MONTH, timesheetView.getMonth());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_YEARS, years);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETDAYS, days);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, sheets);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET, timesheet);
      Date dateApprovedorReviewed = timesheetService.dateLastTimeSheetApprovedorReviewed(ResourcesHelper.getResource());
      String formattedDate = null;
      if(dateApprovedorReviewed != null){
          formattedDate = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME).format(dateApprovedorReviewed);
      }
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_DATE_LASTDAYAPPROVED, formattedDate);
      
//      if(timesheet.getId() != null){
//         timesheetHelper.messageInfo(model, timesheet);
//      }
   
      return ConstantsHelper.VIEW_TIMESHEET_READ;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_PARAMETER_YEAR_MONTH + "/{step}")
   public String loadTab(@PathVariable int year, @PathVariable Integer month, @PathVariable String step, Model model, HttpServletRequest request) {
      timesheetHelper.loadTimesheet(model, year, month);
   
      return ConstantsHelper.VIEW_TIMESHEET_READ;
   }
   
   /*
    * AJAX TAB HOURS
    */
   @ApiOperation("Mapping to respond ajax requests for timesheet hours tab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_READ + "')")
   @GetMapping( value = ConstantsHelper.MAPPING_TIMESHEET_AJAX_HOURS +  ConstantsHelper.MAPPING_PARAMETER_YEAR_MONTH)
   public String hoursTab(Model model, @PathVariable int year, @PathVariable Integer month) {
      List<WorkItemWorkLog> worklogs = workItemWorkLogService.getByYearAndMonthAndResourceOrderByDate(new Year(year), month, ResourcesHelper.getResource());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMLOGS, worklogs);
      
      return ConstantsHelper.VIEW_TIMESHEET_TABHOURS;
   }
   
   /*
    * AJAX TAB SUMMARY
    */
   @ApiOperation("Mapping to respond ajax requests for timesheet summary tab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_READ + "')")
   @GetMapping( value = ConstantsHelper.MAPPING_TIMESHEET_AJAX_SUMMARY +  ConstantsHelper.MAPPING_PARAMETER_YEAR_MONTH)
   public String summaryTab(Model model, @PathVariable int year, @PathVariable Integer month) {
      
      Resource resource = ResourcesHelper.getResource();
      List<Timesheet> sheets = timesheetService.getSheets(resource, new Year(year), month);      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, sheets);       
      return ConstantsHelper.VIEW_TIMESHEET_TABSUMMARY;
   }
   
   /*
    * AJAX TAB ABSENCES
    */
   @ApiOperation("Mapping to respond ajax requests for timesheet absences tab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_READ + "')")
   @GetMapping( value = ConstantsHelper.MAPPING_TIMESHEET_AJAX_ABSENCES +  ConstantsHelper.MAPPING_PARAMETER_YEAR_MONTH)
   public String absencesTab(Model model, @PathVariable int year, @PathVariable Integer month) {
      
      Resource resource = ResourcesHelper.getResource();
      List<Absence> absences = absencesService.listByResourceYearAndMonth(resource, new Year(year), month);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCES, absences);       
      return ConstantsHelper.VIEW_TIMESHEET_TABABSENCES;
   }
   
   /*
    * AJAX TAB VACATIONS
    */
   @ApiOperation("Mapping to respond ajax requests for timesheet vacations tab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_READ + "')")
   @GetMapping( value = ConstantsHelper.MAPPING_TIMESHEET_AJAX_VACATIONS +  ConstantsHelper.MAPPING_PARAMETER_YEAR_MONTH)
   public String vacationsTab(Model model, @PathVariable int year, @PathVariable Integer month) {
      
      Resource resource = ResourcesHelper.getResource();
      List<Vacation> vacations = vacationsService.listByResourceYearAndMonth(resource, new Year(year), month);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATIONS, vacations);      
      return ConstantsHelper.VIEW_TIMESHEET_TABVACATIONS;
   }
   
   /*
    * AJAX TAB COMPENSATION DAYS
    */
   @ApiOperation("Mapping to respond ajax requests for timesheet compensation days tab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_READ + "')")
   @GetMapping( value = ConstantsHelper.MAPPING_TIMESHEET_AJAX_COMPENSATION +  ConstantsHelper.MAPPING_PARAMETER_YEAR_MONTH)
   public String compensationTab(Model model, @PathVariable int year, @PathVariable Integer month) {
      
      List<CompensationDay> compensationdays = compensationDaysService.getByYearAndMonth(new Year(year), month);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_COMPENSATIONDAYS, compensationdays);        
      return ConstantsHelper.VIEW_TIMESHEET_TABCOMPENSATION;
   }
   
   /*
    * AJAX TAB COMMENTS
    */
   @ApiOperation("Mapping to respond ajax requests for timesheet comments tab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_READ + "')")
   @GetMapping( value = ConstantsHelper.MAPPING_TIMESHEET_AJAX_COMMENTS +  ConstantsHelper.MAPPING_PARAMETER_YEAR_MONTH)
   public String commentsTab(Model model, @PathVariable int year, @PathVariable Integer month) {
      
      Resource resource = ResourcesHelper.getResource();
      List<Timesheet> comments = timesheetService.getSheetsWithComments(resource, new Year(year), month);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_COMMENTS, comments);             
      return ConstantsHelper.VIEW_TIMESHEET_TABCOMMENTS;
   }
   
   /*
    * AJAX TAB HOLIDAYS
    */
   @ApiOperation("Mapping to respond ajax requests for timesheet holidays tab.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_READ + "')")
   @GetMapping( value = ConstantsHelper.MAPPING_TIMESHEET_AJAX_HOLIDAYS +  ConstantsHelper.MAPPING_PARAMETER_YEAR_MONTH)
   public String holidaysTab(Model model, @PathVariable int year, @PathVariable Integer month) {
      
      List<Holiday> holidays = holidaysService.getByYearAndMonth(new Year(year), month);
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_HOLIDAYS, holidays);      
      return ConstantsHelper.VIEW_TIMESHEET_TABHOLIDAYS;
   }

   /*
    * TODO: missing preauthorize
    */
   @GetMapping(value = ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_WORKITEMS_WORKLOG_DELETE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String deleteWorklog(Model model,  @PathVariable int id) {
      WorkItemWorkLog workLog = workItemWorkLogService.getOne(id);
//      workItemWorkLogService.delete(worklog);  
//      Time day = worklog.getDay();
      workLogsHelper.remove(id);
      
      List<WorkItemWorkLog> worklogs = workItemWorkLogService.getByYearAndMonthAndResourceOrderByDate(workLog.getDay().getYear(), workLog.getDay().getMonth(), ResourcesHelper.getResource());
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMLOGS, worklogs);        
      
      return ConstantsHelper.VIEW_TIMESHEET_READ + ConstantsHelper.VIEW_FRAGMENT_TIMESHEET;
   }

   /*
    * TODO: missing preauthorize
    */
   @GetMapping(value = ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_VACATIONS_DELETE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String deleteVacation(Model model,  @PathVariable int id) {
//      Vacation vacation = vacationsService.getById(id);
//      vacationsService.delete(vacation) ;  
      
      vacationsHelper.remove(id);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATIONS, vacationsService.listByResourceYearAndMonth(ResourcesHelper.getResource(), new Year(DatesHelper.year()), DatesHelper.month()));  
      
//      timesheetHelper.loadTimesheet(model, vacation.getDay().getYear().getId(), vacation.getDay().getMonth());
      
      return ConstantsHelper.VIEW_TIMESHEET_READ + ConstantsHelper.VIEW_FRAGMENT_TIMESHEET;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_VACATIONS_CREATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_VACATIONS_SAVE)
   public String saveVacation(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATION) @Valid VacationView view) {
      List<Vacation> vacations = vacationsService.listDays(view.translate());      
//      vacationsService.save(vacations);
      vacationsHelper.add(vacations);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_VACATIONS, vacationsService.listByResourceYearAndMonth(ResourcesHelper.getResource(), new Year(DatesHelper.year()), DatesHelper.month()));  
      
      return ConstantsHelper.VIEW_TIMESHEET_TABVACATIONS;
   }

   /*
    * TODO: missing preauthorize
    */
   @GetMapping(value = ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_ABSENCES_DELETE + ConstantsHelper.MAPPING_PARAMETER_ID)
   public String deleteAbsence(Model model,  @PathVariable int id) {
//      Absence absence = absencesService.getById(id);
//      absencesService.delete(absence);
//      
//      Resource resource = ResourcesHelper.getResource();
      
      Absence absence = absencesHelper.remove(id);
      
      List<Absence> absences = absencesService.listByResourceYearAndMonth(ResourcesHelper.getResource(), absence.getDay().getYear(),absence.getDay().getMonth());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCES, absences);
      
//      timesheetHelper.loadTimesheet(model, absence.getDay().getYear().getId(), absence.getDay().getMonth());
      
      return ConstantsHelper.VIEW_TIMESHEET_READ + ConstantsHelper.VIEW_FRAGMENT_TIMESHEET;
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_ABSENCES_CREATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_ABSENCES_SAVE)
   public String saveAbsence(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCE) @Valid AbsenceView view) {
//      Absence absence = absenceView.translate();
//      absencesService.save(absence);
      
      Absence absence = absencesHelper.add(view);
//      Resource resource = ResourcesHelper.getResource();
      List<Absence> absences = absencesService.listByResourceYearAndMonth(ResourcesHelper.getResource(), absence.getDay().getYear(),absence.getDay().getMonth());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_ABSENCES, absences);   
      
//      timesheetHelper.loadTimesheet(model, absence.getDay().getYear().getId(), absence.getDay().getMonth());
      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_SLASH + absence.getDay().getYear().getId().toString() + ConstantsHelper.MAPPING_SLASH + absence.getDay().getMonth().toString() + ConstantsHelper.MAPPING_SLASH + ConstantsHelper.VIEW_ATTRIBUTE_ABSENCES;
   }
   
   /*
    * TODO: missing preauthorize
    */
   @PostMapping(value = ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_WORKITEMS_WORKLOG_SAVE)
   public String workLogSave(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_WORKITEMWORKLOG) @Valid WorkItemWorkLogView view) {
      /*
       * TODO: refactor this method to call helper
       */
//      WorkItemWorkLog workLog = view.translate();
//      workLog = workItemWorkLogService.save(workLog);
      
      WorkItemWorkLog workLog = workLogsHelper.add(view);
      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_SLASH + workLog.getDay().getYear().getId().toString() + ConstantsHelper.MAPPING_SLASH + workLog.getDay().getMonth().toString();
   }
   
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_SUBMIT + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_TIMESHEET_SUBMIT)
   public String submitOrRevert(Model model, RedirectAttributes redirAttrs, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET) @Valid TimesheetView view) {
      Timesheet timesheet = timesheetService.getOne(view.getId());
      
      switch (timesheet.getStatus().getId()) {
         case TimesheetStatus.SUBMITTED:
         case TimesheetStatus.REVIEWED:
            timesheetHelper.revert(timesheet);
            break;
         case TimesheetStatus.NOT_SUBMITTED:
            timesheetHelper.submit(timesheet);
            break;
      }
//      //revert
//      if(timesheet.getStatus().getId() == TimesheetStatus.SUBMITTED || timesheet.getStatus().getId() == TimesheetStatus.REVIEWED) {
//         timesheet.setSubmitted(false);
//         timesheet.setStatus(new TimesheetStatus(TimesheetStatus.NOT_SUBMITTED));
//         
//         timesheet.setReviewed(false);
//         timesheet.setReviewedBy(null);
//         timesheet.setReviewDate(null);
//          
//         timesheet.setApproved(false);
//         timesheet.setApprovedBy(null);
//         timesheet.setApprovalDate(null);
//          
//         timesheetHelper.deleteBillables(timesheetService.save(timesheet));
//         
//      }
//      else { //submit
//         if(timesheetHelper.canSubmit(timesheet)) {
//            timesheet.setApproved(false);
//            timesheet.setReviewed(false);
//            timesheet.setSubmitted(true);
//            timesheet.setStatus(new TimesheetStatus(TimesheetStatus.SUBMITTED));
//            
//            timesheetService.save(timesheet);
//            
//            redirAttrs.addFlashAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE, messagesHelper.createMessage(ConstantsHelper.SUCCESS_MESSAGE, ConstantsHelper.SUCCESS_ICON_MESSAGE, messageSource.getMessage(ConstantsHelper.MESSAGE_VALIDATION_TIMESHEET_SUBMIT, new Object[] {timesheet.getStartingDay().getFortnight(),ConstantsHelper.MONTHS.get(timesheet.getStartingDay().getMonth() -1)}, LocaleContextHolder.getLocale())));
//         }
//         else {        
//            redirAttrs.addFlashAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE, messagesHelper.createMessage(ConstantsHelper.ERROR_MESSAGE, ConstantsHelper.ERROR_ICON_MESSAGE, messageSource.getMessage(ConstantsHelper.MESSAGE_VALIDATION_EMPTY_TIMESHEET_SUBMIT, null, LocaleContextHolder.getLocale())));
//         }
//      }

      return  ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_SLASH + timesheet.getStartingDay().getYear().getId() + ConstantsHelper.MAPPING_SLASH + timesheet.getStartingDay().getMonth();
   }

//   @PostMapping(value = ConstantsHelper.MAPPING_TIMESHEET_REVIEW_VALIDATION, params="action=accept")
//   public String acceptReview(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET) @Valid TimesheetView view, RedirectAttributes redirAttrs) {
//      Timesheet timesheet = timesheetService.getOne(view.getId());
//      
////      timesheetReviewHelper.approve(timesheet, view);
//      timesheetHelper.review(timesheet);
//      
//      /*
//       * TODO: move this code to helper
//       */  
////      timesheetHelper.commentValidation(timesheet, view);
////      timesheet.setReviewed(true);
////      timesheet.setReviewedBy(ResourcesHelper.getResource());
////      timesheet.setReviewDate(DatesHelper.now());
////      Timesheet saved = this.timesheetService.save(timesheet);
////      
////      timesheetHelper.createBillables(saved);
////      
////      redirAttrs.addFlashAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE, messagesHelper.createMessage(ConstantsHelper.SUCCESS_MESSAGE, ConstantsHelper.SUCCESS_ICON_MESSAGE, messageSource.getMessage(ConstantsHelper.MESSAGE_VALIDATION_TIMESHEET_REVIEW, new Object[] {timesheet.getResource().getName(),timesheet.getStartingDay().getFortnight(),timesheet.getStartingDay().getMonth()}, LocaleContextHolder.getLocale())));
//
//      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_REVIEW;
//   }
//  
//   @PostMapping(value = ConstantsHelper.MAPPING_TIMESHEET_REVIEW_VALIDATION, params="action=reject")
//   public String rejectReview(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET) @Valid TimesheetView view, RedirectAttributes redirAttrs) {
//      Timesheet timesheet = timesheetService.getOne(view.getId());
//
////      timesheetReviewHelper.reject(timesheet, view);
//      timesheetHelper.reject(timesheet);
//      
//      /*
//       * TODO: move this code to helper
//       */
////       timesheetHelper.commentValidation(timesheet, timesheetView);
////       timesheet.setSubmitted(false);
////       timesheet.setReviewedBy(ResourcesHelper.getResource());
////       timesheet.setReviewDate(DatesHelper.now());
////       this.timesheetService.save(timesheet);
////       
////       redirAttrs.addFlashAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE, messagesHelper.createMessage(ConstantsHelper.ERROR_MESSAGE, ConstantsHelper.ERROR_ICON_MESSAGE, messageSource.getMessage(ConstantsHelper.MESSAGE_VALIDATION_TIMESHEET_REJECT, new Object[] {timesheet.getResource().getName(),timesheet.getStartingDay().getFortnight(),timesheet.getStartingDay().getMonth()}, LocaleContextHolder.getLocale())));
//
//       return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_TIMESHEET + ConstantsHelper.MAPPING_REVIEW;
//   }
   
   @GetMapping(value = ConstantsHelper.MAPPING_TIMESHEET_PREVIEW + ConstantsHelper.MAPPING_PARAMETER_TIMESHEET)
   public String preview(Model model, @PathVariable Integer timesheet) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEET, timesheetService.getOne(timesheet));
      
      return ConstantsHelper.VIEW_TIMESHEET_PREVIEW;
   }
   
//   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TIMESHEETS_SUBMIT + "')")
//   @GetMapping(value = ConstantsHelper.MAPPING_TIMESHEET_REVERT_SUBMISSION + ConstantsHelper.MAPPING_PARAMETER_ID)
//   public String revertSubmission(Model model, @PathVariable int id) {
//      /*
//       * TODO: move this code to helper
//       */
//      Timesheet timesheet = timesheetService.getOne(id);
////      timesheet.setSubmitted(false);
////      timesheet.setStatus(new TimesheetStatus(TimesheetStatus.NOT_SUBMITTED));
//      timesheetHelper.revert(timesheet);
//      
//      List<Timesheet> timesheets = timesheetService.getSheets(timesheet.getResource(), timesheet.getStartingDay().getYear(), timesheet.getStartingDay().getMonth());      
//      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TIMESHEETS, timesheets);  
//      
//      return ConstantsHelper.VIEW_TIMESHEET_TABSUMMARY;
//   }
//   
   @GetMapping(value = ConstantsHelper.MAPPING_TIMESHEET_SUBMITTED + ConstantsHelper.MAPPING_PARAMETER_DATE)
   @ResponseBody
   public boolean checkTimesheetSubmitted(HttpServletResponse response, @PathVariable String date) throws ParseException {
      return !timesheetService.checkTimesheetSubmitted(new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATE).parse(date));
   }
}

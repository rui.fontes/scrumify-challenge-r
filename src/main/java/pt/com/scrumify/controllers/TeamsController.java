package pt.com.scrumify.controllers;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Optional;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.propertyeditors.CustomDateEditor;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;

import io.swagger.annotations.ApiOperation;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.services.SubAreaService;
import pt.com.scrumify.database.services.TeamService;
import pt.com.scrumify.entities.TeamView;
import pt.com.scrumify.helpers.ConstantsHelper;
import pt.com.scrumify.helpers.MessagesHelper;
import pt.com.scrumify.helpers.ResourcesHelper;
import pt.com.scrumify.helpers.TeamsHelper;

@Controller
public class TeamsController {
   
   @Autowired
   private MessagesHelper messagesHelper;
   @Autowired
   private MessageSource messageSource;
   @Autowired
   private TeamsHelper teamsHelper;
   @Autowired
   private TeamService teamsService;
   @Autowired
   private SubAreaService subAreaService;

   
   @InitBinder
   public void initBinder(WebDataBinder binder) {
      SimpleDateFormat format = new SimpleDateFormat(ConstantsHelper.DEFAULT_FORMAT_DATETIME);
      format.setLenient(true);
      
      binder.registerCustomEditor(Date.class, new CustomDateEditor(format, true));
   }
     
   /*
    * LIST
    */ 
   @ApiOperation("Mapping to list the teams of a resource.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_TEAMS)
   public String list(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, this.teamsService.getByMember(ResourcesHelper.getResource()));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_MESSAGE, this.messagesHelper.createMessage(ConstantsHelper.INFO_MESSAGE, ConstantsHelper.INFO_ICON_MESSAGE, messageSource.getMessage(ConstantsHelper.MESSAGE_VALIDATION_EMPTY_TEAMS, null, LocaleContextHolder.getLocale())));
      
      return ConstantsHelper.VIEW_TEAMS_READ;
   }
   
   @ApiOperation("Mapping to list the teams of a resource.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_READ + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_TEAMS_ACTIVE + ConstantsHelper.MAPPING_PARAMETER_ACTIVEFLAG)
   public String listActive(Model model, @PathVariable String activeFlag) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAMS, teamsHelper.getBasedOnActiveFlag(activeFlag));
      
      return ConstantsHelper.VIEW_TEAMS_READ + " :: list-teams";
   }
  
   /*
    * CREATE
    */  
   @ApiOperation("Mapping to create a Team.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_CREATE + "')")
   @GetMapping(value = ConstantsHelper.MAPPING_TEAMS_CREATE)
   public String create(Model model) {
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM , new TeamView());
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, this.subAreaService.getAll());
      
      return ConstantsHelper.VIEW_TEAMS_CREATE;
   }
   
   /*
    * UPDATE
    */
   @ApiOperation("Mapping to update a Team.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_READ + "') and @SecurityService.isMember('Team', #team)")
   @GetMapping(value = {ConstantsHelper.MAPPING_TEAMS_UPDATE, ConstantsHelper.MAPPING_TEAMS_UPDATE + ConstantsHelper.MAPPING_PARAMETER_TAB})
   public String update(Model model, @PathVariable int team, @PathVariable Optional<String> tab) {
      TeamView teamView = new TeamView(this.teamsService.getOne(team));      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, teamView);
      
      return ConstantsHelper.VIEW_TEAMS_UPDATE;
   }
   
   /*
    * SAVE
    */
   @ApiOperation("Mapping to save a Team.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_CREATE + "')")
   @PostMapping(value = ConstantsHelper.MAPPING_TEAMS_SAVE)
   public String save(Model model, @ModelAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM) @Valid TeamView teamView, BindingResult bindingResult) {
      if (bindingResult.hasErrors()) {         
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, teamView);
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, this.subAreaService.listSubarea(teamView.getSubArea()));
         
         return ConstantsHelper.VIEW_TEAMS_CREATE;
      }  
      this.teamsHelper.save(teamView);
      
      return ConstantsHelper.MAPPING_REDIRECT + ConstantsHelper.MAPPING_TEAMS;
   }

   /*
    * ARCHIVE
    */
   @ApiOperation("Mapping to archive a Team.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_UPDATE + "') and @SecurityService.isMember('Team', #teamid)")
   @GetMapping(value = ConstantsHelper.MAPPING_TEAMS_ARCHIVE + "/{teamid}")
   public String archive(Model model, @PathVariable int teamid) {
      Team team = this.teamsService.getOne(teamid);
      if(teamsHelper.existDependencies(team) && team.isActive()) {
         model.addAttribute("message", "There are Workitems, Epics or User Stories associated to this team. Decouple them first then archive team.");
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, new TeamView(team));
         
      } else {
         model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, teamsService.archive(team));
      }

      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, this.subAreaService.listSubarea(team.getSubArea()));
      return ConstantsHelper.VIEW_TEAMS_INFO + " :: team";
   }

   /*
    * TECHNICAL ASSISTANCE
    */
   @ApiOperation("Mapping to update technical assistance flag of a Team.")
   @PreAuthorize(ConstantsHelper.SECURITY_HAS_AUTHORITY + ConstantsHelper.ROLE_TEAMS_UPDATE + "') and @SecurityService.isMember('Team', #teamid)")
   @GetMapping(value = ConstantsHelper.MAPPING_TEAMS_TECHNICAL_ASSISTANCE + "/{teamid}")
   public String technicalAssistance(Model model, @PathVariable int teamid) {
      Team team = this.teamsService.getOne(teamid);
      team.setTechnicalAssistance(!team.isTechnicalAssistance());
      this.teamsService.save(team);
      
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_TEAM, new TeamView(team));
      model.addAttribute(ConstantsHelper.VIEW_ATTRIBUTE_SUBAREAS, this.subAreaService.listSubarea(team.getSubArea()));
      
      return ConstantsHelper.VIEW_TEAMS_INFO + " :: team";
   }
}
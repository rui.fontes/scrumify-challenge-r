package pt.com.scrumify.database.repositories;

import java.util.Date;
import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.entities.Year;

@Repository
public interface WorkItemWorkLogRepository extends JpaRepository<WorkItemWorkLog, Integer> {
   
   WorkItemWorkLog findByIdAndCreatedBy(Integer id, Resource resource);
   
   List<WorkItemWorkLog> findByWorkItemContract(Contract contract);
   List<WorkItemWorkLog> findByCreatedBy(Resource resource);
   List<WorkItemWorkLog> findByCreatedByAndWorkItemContract(Resource resource, Contract contract);
   
   @Query(value = "SELECT wlog " + 
                  "FROM WorkItemWorkLog wlog " +
                  "JOIN wlog.workItem wi " +
                  "WHERE wlog.createdBy = :resource " + 
                  "AND wlog.day.year = :year " + 
                  "AND wlog.day.month = :month " +
                  "GROUP BY wlog.id, wi.contract.code, wlog.typeOfWork " +
                  "ORDER BY wlog.day")
   List<WorkItemWorkLog> getByYearAndMonthAndResourceOrderByDate(@Param("year") Year year, @Param("month") Integer month, @Param("resource") Resource resource);

   
   @Query(value = "SELECT wlog " + 
         "FROM WorkItemWorkLog wlog " +
         "JOIN wlog.workItem wi " +
         "WHERE wlog.createdBy = :resource " + 
         "AND wlog.day.year = :year " + 
         "AND wlog.day.month = COALESCE(:month,wlog.day.month) " +
         "AND wlog.day.fortnight = COALESCE(:fortnight,wlog.day.fortnight) " + 
         "GROUP BY wlog.id " +
         ", wi.contract.code " +
         ", wlog.typeOfWork " +
         "ORDER BY  wi.contract.code " +
         ", wlog.typeOfWork "  
         )
   List<WorkItemWorkLog> getByYearAndMonthAndResourceAndFortnightOrderByDate(@Param("year") Year year, @Param("month") Integer month, @Param("resource") Resource resource, @Param("fortnight") Integer fortnight);

   
   @Query(value = "SELECT wlog " + 
         "FROM WorkItemWorkLog wlog " +
         "JOIN wlog.workItem wi " +
         "WHERE wlog.createdBy In( :resources) " + 
         "AND wlog.day.year = :year " + 
         "AND wlog.day.month = COALESCE(:month,wlog.day.month) " +
         "AND wlog.day.fortnight = COALESCE(:fortnight,wlog.day.fortnight) " + 
         "GROUP BY wlog.id " +
         ", wi.contract.code " +
         ", wlog.typeOfWork " +
         "ORDER BY  wi.contract.code " +
         ", wlog.typeOfWork "  
         )
   List<WorkItemWorkLog> getByYearAndMonthAndResourcesAndFortnightOrderByDate(@Param("year") Year year, @Param("month") Integer month, @Param("resource") List<Resource> resources, @Param("fortnight") Integer fortnight);
 
   @Query(value = "SELECT wlog " + 
         "FROM WorkItemWorkLog wlog " +
         "JOIN wlog.workItem wi " +
         "WHERE wlog.createdBy = :resource " + 
         "AND wlog.day.year = :year " + 
         "AND wi.contract = :contract " + 
         "AND wlog.day.month = COALESCE(:month,wlog.day.month) " +
         "AND wlog.day.fortnight = COALESCE(:fortnight,wlog.day.fortnight) " + 
         "GROUP BY wlog.id " +
         ", wi.contract.code " +
         ", wlog.typeOfWork " +
         "ORDER BY  wi.contract.code " +
         ", wlog.typeOfWork "  
         )
   List<WorkItemWorkLog> getByYearAndMonthAndResourcesAndFortnightAndContractOrderByDate(@Param("year") Year year, @Param("month") Integer month, @Param("resource") Resource resource, @Param("fortnight") Integer fortnight, @Param("contract") Contract contract);
   
   @Query(value = "SELECT sum(wlog.timeSpent) " +
                  "FROM WorkItemWorkLog wlog " +
                  "JOIN wlog.workItem.contract c " +
                  "WHERE c = :contract " +
                  "AND wlog.created between :start and :end ")
   Integer getHoursSpentDuringInterval(@Param("contract") Contract contract, @Param("start") Date start, @Param("end") Date end);
   
   @Query(value = "SELECT sum(wlog.timeSpent) " +
                  "FROM WorkItemWorkLog wlog " +
                  "JOIN wlog.workItem w " +
                  "WHERE w = :workitem " +
                  "AND wlog.id <> :worklog ")
   Integer getHoursSpentOnWorkitemAndNotWorklog(@Param("workitem") WorkItem workitem, Integer worklog);
}
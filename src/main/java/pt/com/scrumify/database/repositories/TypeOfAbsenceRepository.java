package pt.com.scrumify.database.repositories;

import java.util.List;

import org.springframework.data.jpa.repository.JpaRepository;

import pt.com.scrumify.database.entities.TypeOfAbsence;


public interface TypeOfAbsenceRepository extends JpaRepository<TypeOfAbsence, Integer> {
   TypeOfAbsence findByCodeAndName(String code, String name);
   List<TypeOfAbsence> findAllByOrderByNameAsc();
}
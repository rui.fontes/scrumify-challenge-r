package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Sort;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.TypeOfTimesheetApproval;
import pt.com.scrumify.database.repositories.TypeOfTimesheetBillableRepository;

@Service
public class TypeOfTimesheetBillableServiceImpl implements TypeOfTimesheetBillableService {
   
   @Autowired
   private TypeOfTimesheetBillableRepository repository;

   @Override
   public TypeOfTimesheetApproval getOne(Integer id) {
      return repository.getOne(id);
   }

   @Override
   public List<TypeOfTimesheetApproval> listAll() {
      return repository.findAll(Sort.by("name"));
   }
}
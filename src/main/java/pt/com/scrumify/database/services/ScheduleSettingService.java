package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.ScheduleSetting;

public interface ScheduleSettingService {
   
   ScheduleSetting getOne(Integer id);
   ScheduleSetting save(ScheduleSetting setting);	
   void delete(ScheduleSetting setting);
   List<ScheduleSetting> getAll();
   
}
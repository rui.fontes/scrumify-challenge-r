package pt.com.scrumify.database.services;

import java.util.List;
import pt.com.scrumify.database.entities.Sprint;

public interface SprintService {
   Sprint getOne(Integer id);
   Sprint save(Sprint area);
   List<Sprint> listAll();
   void delete(Sprint area);
   
}
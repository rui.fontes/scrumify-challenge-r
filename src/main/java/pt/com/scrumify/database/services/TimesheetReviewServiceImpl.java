package pt.com.scrumify.database.services;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.TimesheetReview;
import pt.com.scrumify.database.entities.Year;
import pt.com.scrumify.database.repositories.TimesheetReviewRepository;

@Service
public class TimesheetReviewServiceImpl implements TimesheetReviewService {
   @Autowired
   private TimesheetReviewRepository repository;

   @Override
   public TimesheetReview getOne(Integer id) {
      return repository.getOne(id);
   }
   
   @Override
   public TimesheetReview getOne(Resource resource, Year year, Integer month, Integer fortnight, String code, String description) {
      return repository.getOne(resource, year, month, fortnight, code, description);
   }

   @Override
   public TimesheetReview save(TimesheetReview entity) {
      return repository.save(entity);
   }

   @Override
   public void delete(Integer id) {
      repository.deleteById(id);
   }

   @Override
   public List<TimesheetReview> getByTimesheet(Timesheet timesheet) {
      return repository.getByTimesheet(timesheet);
   }
}
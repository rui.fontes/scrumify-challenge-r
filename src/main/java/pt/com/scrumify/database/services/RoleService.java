package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.Role;

public interface RoleService {
   Role getOne(Integer id);
   Role save(Role role);
   List<Role> getAll();
   List<Role> getAllOrderedByName();
}
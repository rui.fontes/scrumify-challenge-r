package pt.com.scrumify.database.services;

import java.util.List;
import pt.com.scrumify.database.entities.TypeOfAbsence;

public interface TypeOfAbsenceService {
   List<TypeOfAbsence> getAllTypes();
   TypeOfAbsence findByCodeAndName(String code, String name);
}
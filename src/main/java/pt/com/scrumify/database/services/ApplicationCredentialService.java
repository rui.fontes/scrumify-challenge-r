package pt.com.scrumify.database.services;

import java.util.List;

import pt.com.scrumify.database.entities.ApplicationCredential;

public interface ApplicationCredentialService {
   void delete(ApplicationCredential credencial);
   List<ApplicationCredential> getByApplicationAndEnvironment(int application, int environment);
   ApplicationCredential getOne(int credential, int application, int environment);
   ApplicationCredential save(ApplicationCredential credential);
}
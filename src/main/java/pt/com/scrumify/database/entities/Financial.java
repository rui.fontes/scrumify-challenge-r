package pt.com.scrumify.database.entities;

import java.io.Serializable;
import java.math.BigDecimal;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import pt.com.scrumify.helpers.ConstantsHelper;

@Entity
@NoArgsConstructor
@Table(name = ConstantsHelper.DATABASE_TABLE_FINANCIALS)
public class Financial implements Serializable {
   private static final long serialVersionUID = 6904399980203715368L;

   @Id
   @Getter
   @Setter
   @GeneratedValue(strategy = GenerationType.IDENTITY)
   @Column(name = "id", nullable = false)
   private Integer id = 0;

   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "contract", nullable = false)
   private Contract contract;
 
   @Getter
   @Setter
   @ManyToOne(fetch = FetchType.LAZY)
   @JoinColumn(name = "time", nullable = false)
   private Time time;
   
   @Getter
   @Setter
   @Column(name = "type", length = 50, nullable = false)
   private String type;
   
   @Getter
   @Setter
   @Column(name = "category", length = 50, nullable = false)
   private String category;
   
   @Getter
   @Setter
   @Column(name = "enterpriseid", length = 50, nullable = false)
   private String enterpriseId;
   
   @Getter
   @Setter
   @Column(name = "actual", nullable = true)
   private boolean actual;
   
   @Getter
   @Setter
   @Column(name = "tbdr", nullable = true)
   private boolean tbdr;
   
   @Getter
   @Setter
   @Column(name = "value", nullable = true)
   private BigDecimal value;
}
package pt.com.scrumify.entities;

import java.util.Date;

import lombok.Getter;
import lombok.Setter;

public class PairIntegerDate {
   @Getter
   @Setter
   private int index;
   
   @Getter
   @Setter
   private Date value;
   
   public PairIntegerDate(int index, Date value) {
      this.index = index;
      this.value = value;
   }
}
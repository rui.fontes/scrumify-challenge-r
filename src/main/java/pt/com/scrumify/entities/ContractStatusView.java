package pt.com.scrumify.entities;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.ContractStatus;

public class ContractStatusView {
   
   @Getter
   @Setter
   private Integer id;
   
   @Getter
   @Setter
   private String name;
   
   public ContractStatusView() {
      super();
   }

   public ContractStatusView(ContractStatus status) {
      super();
      
      this.id = status.getId();
      this.name = status.getName();
   }   
}
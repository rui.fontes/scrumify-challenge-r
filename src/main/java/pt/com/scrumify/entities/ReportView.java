package pt.com.scrumify.entities;

import java.util.Date;
import java.util.List;

import lombok.Getter;
import lombok.Setter;
import pt.com.scrumify.database.entities.Area;
import pt.com.scrumify.database.entities.Contract;
import pt.com.scrumify.database.entities.Resource;
import pt.com.scrumify.database.entities.SubArea;
import pt.com.scrumify.database.entities.Team;
import pt.com.scrumify.database.entities.WorkItemStatus;
import pt.com.scrumify.database.entities.Year;

public class ReportView {
   
   @Getter
   @Setter
   private String type;
   
   @Getter
   @Setter
   private Year year;
   
   @Getter
   @Setter
   private Team team;
   
   @Getter
   @Setter
   private List<Team> teams;
   
   @Getter
   @Setter
   private Resource resource;
   
   @Getter
   @Setter
   private Area area;
   
   @Getter
   @Setter
   private SubArea subArea;
   
   @Getter
   @Setter
   private List<SubArea> subAreas;
   
   @Getter
   @Setter
   private Integer month;
   
   @Getter
   @Setter
   private Integer fortnight;
   
   @Getter
   @Setter
   private Contract contract;
   
   @Getter
   @Setter
   private List<Contract> contracts;
   
   @Getter
   @Setter
   private Date startDay;
   
   @Getter
   @Setter
   private Date endDay;

   @Getter
   @Setter
   private List<WorkItemStatus> status;
   
   public ReportView() {
      super();
   }
}
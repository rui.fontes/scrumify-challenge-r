package pt.com.scrumify.helpers;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import pt.com.scrumify.database.entities.Time;
import pt.com.scrumify.database.entities.Timesheet;
import pt.com.scrumify.database.entities.WorkItem;
import pt.com.scrumify.database.entities.WorkItemWorkLog;
import pt.com.scrumify.database.services.TimeService;
import pt.com.scrumify.database.services.TimesheetService;
import pt.com.scrumify.database.services.WorkItemService;
import pt.com.scrumify.database.services.WorkItemWorkLogService;
import pt.com.scrumify.entities.WorkItemWorkLogView;

@Service
public class WorkItemWorkLogsHelper {
   @Autowired
   private TimeService timeService;
   @Autowired
   private TimesheetService timesheetService;
   @Autowired
   private WorkItemService workItemService;
   @Autowired
   private WorkItemWorkLogService workLogService;
   @Autowired
   private TimesheetDayHelper timesheetDayHelper;
   @Autowired
   private TimesheetReviewHelper timesheetReviewHelper;
   @Autowired
   private WorkItemsHelper workItemHelper;
   
   @Transactional
   public WorkItemWorkLog add(WorkItemWorkLogView view) {
      WorkItemWorkLog workLog = view.translate();
      WorkItem db = workItemService.getOne(view.getWorkItem().getId());
      WorkItem copy = db.clone();

      /*
       * Get Time
       */
      Time time = timeService.getbyDate(view.getDay());
      
      /*
       * Get timesheet
       */
      Timesheet timesheet = timesheetService.getSheetByFortnight(ResourcesHelper.getResource(), time.getYear().getId(), time.getMonth(), time.getFortnight());
      workLog.setTimesheet(timesheet);
      
      /*
       * Check if is a new worklog or worklog update
       */
      if (view.getId() == 0) {         
         /*
          * Save worklog
          */
         workLog = workLogService.save(workLog);
         
         /*
          * Increment hours on workitem
          */
         copy.incrementHours(workLog.getTimeSpent());
         
         /*
          * Add hours to timesheet day
          */
         timesheetDayHelper.addTimeSpent(copy.getAssignedTo(), workLog.getDay().getDate(), workLog.getTimeSpent());
         
         /*
          * Add hours to timesheet review
          */
         timesheetReviewHelper.add(view.getTypeOfWork().getId(), view.getTimeSpent(), workLog.getDay().getYear(), workLog.getDay().getMonth(), workLog.getDay().getFortnight(), copy.getContract(), copy.getContract().getCode(), copy.getContract().getName());
      }
      else {
         WorkItemWorkLog previousWorkLog = workLogService.getOne(view.getId()).clone();
         
         /*
          * Save worklog
          */
         workLog = workLogService.save(workLog);
         
         /*
          * Adjust hours on workitem (decrement previous value and increment the new value)
          */
         copy.decrementHours(previousWorkLog.getTimeSpent());
         copy.incrementHours(workLog.getTimeSpent());
         
         /*
          * Adjust hours on timesheet day (decrement previous value and increment the new value)
          */
         timesheetDayHelper.removeTimeSpent(copy.getAssignedTo(), previousWorkLog.getDay().getDate(), previousWorkLog.getTimeSpent());
         timesheetDayHelper.addTimeSpent(copy.getAssignedTo(), workLog.getDay().getDate(), workLog.getTimeSpent());
         
         /*
          * Adjust hours on timesheet review (decrement previous value and increment the new value)
          */
         timesheetReviewHelper.remove(previousWorkLog.getTypeOfWork().getId(), previousWorkLog.getDay().getYear(), previousWorkLog.getDay().getMonth(), previousWorkLog.getDay().getFortnight(), copy.getContract().getCode(), copy.getContract().getName(), previousWorkLog.getTimeSpent());
         timesheetReviewHelper.add(workLog.getTypeOfWork().getId(), workLog.getTimeSpent(), workLog.getDay().getYear(), workLog.getDay().getMonth(), workLog.getDay().getFortnight(), copy.getContract(), copy.getContract().getCode(), copy.getContract().getName());
      }
      
      /*
       * Set etc of the workitem
       */
      copy.setEtc(view.getEtc());
      
      /*
       * Save workitem
       */
      workItemHelper.save(db, copy);
      
      return workLog;
   }
   
//   @Transactional
//   public void approve(Timesheet timesheet) {
//      List<WorkItemWorkLog> worklogs = workLogService.getByYearAndMonthAndResourceAndFortnighOrderByDate(timesheet.getStartingDay().getYear(), timesheet.getStartingDay().getMonth(), timesheet.getResource(), timesheet.getStartingDay().getFortnight());
//      
//      for (WorkItemWorkLog worklog : worklogs) {
//         worklog.setTimesheet(timesheet);
//      }
//      
//      workLogService.saveAll(worklogs);
//   }
   
   @Transactional
   public WorkItem remove(Integer id) {
      WorkItemWorkLog workLog = workLogService.getOne(id);
      WorkItem db = workItemService.getOne(workLog.getWorkItem().getId());
      WorkItem copy = db.clone();

      /*
       * Decrease hours and increase etc on workitem
       */
      copy.decrementHours(workLog.getTimeSpent());
      copy.setEtc(copy.getEtc() + workLog.getTimeSpent());

      /*
       * Decrease hours on timesheet day
       */
      timesheetDayHelper.removeTimeSpent(copy.getAssignedTo(), workLog.getDay().getDate(), workLog.getTimeSpent());

      /*
       * Decrease hours on timesheet review
       */
      timesheetReviewHelper.remove(workLog.getTypeOfWork().getId(), workLog.getDay().getYear(), workLog.getDay().getMonth(), workLog.getDay().getFortnight(), copy.getContract().getCode(), copy.getContract().getName(), workLog.getTimeSpent());
      
      /*
       * Delete worklog
       */
      workLogService.delete(workLog);
      
      return workItemHelper.save(db, copy);
   }
}
$(document).ready(function() {
   initialize();
});
        
function initialize() {
	
	$('.ui.radio.checkbox').checkbox();
	$('.ui.toggle.checkbox').checkbox();
	$('select.dropdown').dropdown();
	
	formValidation();
}

function formSubmission() {
   var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_PROPERTIES_SAVE}}]];
   
   ajaxpost(url, $("#properties-form").serialize(), "", false, function() {
      initialize();
   });
}

function formValidation() {
   $('form').form({
      on : 'blur',
      inline : true,
      fields : {
    	  name : {
            identifier : 'name',
            rules : [ {
               type : 'empty',
               prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
            } ]
         },
         group : {
          identifier : 'group',
          rules : [ {
             type : 'empty',
             prompt : [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
          } ]
       }
      },
      onSuccess : function(event) {
         event.preventDefault();
         
         formSubmission();
      }
   });
}
$(document).ready(function() {
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	
	formValidation();
	validateDates();
	
	var id = $("input[type=hidden][name='id']").val();
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SPRINTS_CREATE}}]];
	
	if(id != 0){	
		url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SPRINTS}}]] + '/' + id;
		
		ajaxtab(true, url, false, function(settings) {
			switch(settings.urlData.tab) {
				case "tasks":
					getTasks();
					break;
				case "members":
					getMembers();
					break;
				case "reports":
					getReports();
					break;
				case "dashboard":
				default:
					getDashboard();
					break;
			}
		});
	}
	
});

function initialze(){
	$('.ui.dropdown').dropdown();
	$('select.dropdown').dropdown();
	$('.ui.mini').popup({
	       position: 'top center'
	});
}

function getTasks() {
	var id = $("input[type=hidden][name='id']").val();
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SPRINTS}}]] + '/' + id + '/tasks';
   
	ajaxget(url, "div[data-tab='tasks']", function() {           
		initialze();
	});
}

function getMembers() {
	var id = $("input[type=hidden][name='id']").val();
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SPRINTS}}]] + '/' + id + '/members';
   
	ajaxget(url, "div[data-tab='members']", function() {           
		initialze();
	});
}

function getReports() {
	var id = $("input[type=hidden][name='id']").val();
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SPRINTS}}]] + '/' + id + '/reports';
   
	ajaxget(url, "div[data-tab='reports']", function() {           
		initialze();
		var ctx = document.getElementById('chartjs-0');
		if(ctx != null){
			var myChart = new Chart(ctx, {
			    "type": "line",
			    "data": {
			        "labels": ["Day 1", "Day 2", "Day 3", "Day 4", "Day 5", "Day 6", "Day 7", "Day 8", "Day 9", "Day 10"],
			        "datasets": [{
			            "data": [25, 21, 21, 15, 15, 10, 10, 5, 5 , 0],
			            "fill": false,
			            "borderColor": "rgb(178,34,34)",
			            "lineTension": 0.1
			        }]
			    },
			    "options": {
			    	legend: {
			            display: false
			    	}
			    }
			});
		}
	});
}

function getDashboard() {
	var id = $("input[type=hidden][name='id']").val();
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SPRINTS}}]] + '/' + id + '/dashboard';
   
	ajaxget(url, "div[data-tab='dashboard']", function() {           
		initialze();
	});
}


function formValidation() {
   $('#sprints-form').form({
      on : 'blur',
      inline : true,
      fields : {
           name : {
               identifier : 'name',
               rules : [ {
                  type : 'empty',
                  prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               } ]
            },
            team : {
               identifier : 'team',
               rules : [ {
                  type : 'empty',
                  prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
               } ]
            },
            estimatedStart : {
	               identifier : 'estimatedStart',
	               rules : [ {
	                  type : 'empty',
	                  prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	               } ]
	            },
	            estimatedEnd : {
	               identifier : 'estimatedEnd',
	               rules : [ {
	                  type : 'empty',
	                  prompt :  [[#{${T(pt.com.scrumify.helpers.ConstantsHelper).MESSAGE_VALIDATION_EMPTY}}]]
	               } ]
	            }
            
      },           	  
      onSuccess : function(event) {
         
      }
   });
}


function validateDates(){	
	var localizedtext = getjson([[@{${T(pt.com.scrumify.helpers.ConstantsHelper).SCRIPT_CALENDAR_LOCALIZATION_JSON}} + ${#locale.language} + '.json']]);
	
	var minDate = $("input[type=hidden][id='minDate']").val();
	var maxDate = $("input[type=hidden][id='maxDate']").val();
   
	calendar('estimatedStart', 'date', localizedtext, minDate, maxDate, 'estimatedEnd');
}

function assignTaskToSprint(sprint, task){
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SPRINTS}}]] +'/' + sprint + [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TASKS_ADD}}]] + '/' + task;
	ajaxget(url, "div[data-tab='tasks']", function() {		 
	});
}

function removeTaskFromSprint(sprint, task){
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SPRINTS}}]] +'/' + sprint + [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_TASKS_REMOVE}}]] + '/' + task;
	ajaxget(url, "div[data-tab='tasks']", function() {		 
	});
}

function addMember(sprint, resource){
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SPRINTS}}]] +'/' + sprint + [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RESOURCES_ADD}}]] + '/' + resource;
	ajaxget(url, "div[data-tab='members']", function() {
		initialze();
	});
}

function removeMember(sprint, resource){
	var url = [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_SPRINTS}}]] +'/' + sprint + [[@{${T(pt.com.scrumify.helpers.ConstantsHelper).MAPPING_RESOURCES_REMOVE}}]] + '/' + resource;
	ajaxget(url, "div[data-tab='members']", function() {
		initialze();
	});
}